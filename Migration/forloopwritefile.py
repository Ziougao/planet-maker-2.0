import numpy as np 
from csv import reader
from csv import writer

iterations = 30

for i in range(iterations):
    import csv
    from csv import reader
    from csv import writer
    input_file = "/Users/ziougao/Desktop/FAKEpopu_tot_" + str(i + 1) + ".csv"
    imported = open(input_file)

    imported_float = reader(imported)
    imported_list = list(imported_float)
    imported_array = np.array(imported_list, dtype=float)

    x1 = np.random.rand(320,1)
    changed_file = imported_array + x1

    output_file = np.savetxt("/Users/ziougao/Desktop/FAKEpopu_tot_" + str(i + 2) + ".csv", changed_file, delimiter=",")

    input_file = output_file
    


