import numpy as np
import csv
from csv import reader

input_file_2 = open(r"/Users/ziougao/Desktop/Houdini_basefiles_tiger/Referenced_files/Jobs_Total_1.csv") #import attraction mass for the Gravity Model
attr_mass_list = list(csv.reader(input_file_2))
attr_mass_array = np.array(attr_mass_list, dtype=float)

iterations = 30
for iter in range(iterations):

     ###################### AUTOMATION #########################

    """
    Each iteration 2% of Jobs will become automated = less Jobs 
    """
    percent_per_year = 1

    for row in range(len(attr_mass_array)):
        current_jobs = attr_mass_array[row]
        attr_mass_array[row] = current_jobs * (1 - percent_per_year/100)
        attr_mass_array = np.array(attr_mass_array, dtype=float)

    print(attr_mass_array[1])

def Automation(Percentage, triangle):
    """
    will decrease jobs amount by Percentage because they become automated
    Attr_Array = Jobs Total
    Percentage = Percentage of Jobs becoming automated

    Per iteration 2% of current Jobs will become automated
    Percentage gives possibility to manually add automation
    """
    
    triangle_tbf = attr_mass_array[triangle]
    attr_mass_array[triangle] = triangle_tbf * (1-(Percentage/100))

    return attr_mass_array

Automation(50,1)

print(attr_mass_array[1])



