This is the read_me, read it!

Use gitlab for sharing code, houdini files and some zips. You can also do other
files but they don't work nicely in gitlab, so better use Gdrive. 

Step by step guide to get gitlab running on your own pc: (locally)
1. install git (using Hans' link on brightspace, both mac and pc)
    it's a back-end thing, so it's normal that you don't see it after installing
    for mac users, open terminal and try git (if a manual shows up, you're good)
2. type 
    git clone https://gitlab.com/Ziougao/planet-maker-2.0/ 